/* @(#)blake2s.h	1.0 04/21/22 2022 N. Sonack */
/*
 * Blake2s implementation taken from the RFC7693
 *
 * Portions Copyright (c) 2022 N. Sonack
 */

#ifndef	_SCHILY_BLAKE2S_H
#define	_SCHILY_BLAKE2S_H

#ifndef _SCHILY_MCONFIG_H
#include <schily/mconfig.h>
#endif
#include <schily/utypes.h>

/* BLAKE2s Hashing Context and API Prototypes */

/* state context */
typedef struct {
	UInt8_t		b[64];         /* input buffer */
	UInt32_t	h[8];          /* chained state */
	UInt32_t	t[2];          /* total number of bytes */
	size_t		c;             /* pointer for b[] */
	size_t		outlen;        /* digest size */
} blake2s_ctx;

/* Message digest sizes as defined per RFC 7693 section 4 */
#define BLAKE2S_128_DIGEST_LENGTH 16
#define BLAKE2S_160_DIGEST_LENGTH 20
#define BLAKE2S_224_DIGEST_LENGTH 28
#define BLAKE2S_256_DIGEST_LENGTH 32

#ifdef	__cplusplus
extern "C" {
#endif

extern int	blake2s_init	__PR((blake2s_ctx *ctx, size_t outlen,
				      const void *key, size_t keylen));
extern void	blake2s_update	__PR((blake2s_ctx *ctx, const void *in, size_t inlen));
extern void	blake2s_final	__PR((blake2s_ctx *ctx, void *out));

/* Convenience functions for mdigest(1L) */
extern int	blake2s128_init	__PR((blake2s_ctx *ctx));
extern int	blake2s160_init	__PR((blake2s_ctx *ctx));
extern int	blake2s224_init	__PR((blake2s_ctx *ctx));
extern int	blake2s256_init	__PR((blake2s_ctx *ctx));

extern void	blake2s128_final	__PR((UInt8_t[BLAKE2S_128_DIGEST_LENGTH], blake2s_ctx *ctx));
extern void	blake2s160_final	__PR((UInt8_t[BLAKE2S_160_DIGEST_LENGTH], blake2s_ctx *ctx));
extern void	blake2s224_final	__PR((UInt8_t[BLAKE2S_224_DIGEST_LENGTH], blake2s_ctx *ctx));
extern void	blake2s256_final	__PR((UInt8_t[BLAKE2S_256_DIGEST_LENGTH], blake2s_ctx *ctx));

#ifdef	__cplusplus
}
#endif

#endif	/* _SCHILY_BLAKE2S_H */

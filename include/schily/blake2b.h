/* @(#)blake2b.h	1.0 04/21/22 2022 N. Sonack */
/*
 * Blake2b implementation taken from the RFC7693
 *
 * Portions Copyright (c) 2022 N. Sonack
 */

#ifndef	_SCHILY_BLAKE2B_H
#define	_SCHILY_BLAKE2B_H

#ifndef _SCHILY_MCONFIG_H
#include <schily/mconfig.h>
#endif
#include <schily/utypes.h>

/* BLAKE2b Hashing Context and API Prototypes */

/* state context */
typedef struct {
	UInt8_t b[128];                     /* input buffer */
	UInt64_t h[8];                      /* chained state */
	UInt64_t t[2];                      /* total number of bytes */
	size_t c;                           /* pointer for b[] */
	size_t outlen;                      /* digest size */
} blake2b_ctx;

/* As per section 4 of RFC 7693, these are the digest lengths */
#define BLAKE2B_160_DIGEST_LENGTH 20
#define BLAKE2B_256_DIGEST_LENGTH 32
#define BLAKE2B_384_DIGEST_LENGTH 48
#define BLAKE2B_512_DIGEST_LENGTH 64

#ifdef	__cplusplus
extern "C" {
#endif

extern int	blake2b_init	__PR((blake2b_ctx *ctx, size_t outlen,
				      const void *key, size_t keylen));
extern void	blake2b_update	__PR((blake2b_ctx *ctx,
				      const void *in, size_t inlen));
extern void	blake2b_final	__PR((blake2b_ctx *ctx, void *out));
extern int	blake2b		__PR((void *out, size_t outlen,
				      const void *key, size_t keylen,
				      const void *in, size_t inlen));

/* Convenience functions for mdigest(1L) */
extern int	blake2b160_init		__PR((blake2b_ctx *ctx));
extern int	blake2b256_init		__PR((blake2b_ctx *ctx));
extern int	blake2b384_init		__PR((blake2b_ctx *ctx));
extern int	blake2b512_init		__PR((blake2b_ctx *ctx));

extern void	blake2b160_final	__PR((UInt8_t[BLAKE2B_160_DIGEST_LENGTH], blake2b_ctx *ctx));
extern void	blake2b256_final	__PR((UInt8_t[BLAKE2B_256_DIGEST_LENGTH], blake2b_ctx *ctx));
extern void	blake2b384_final	__PR((UInt8_t[BLAKE2B_384_DIGEST_LENGTH], blake2b_ctx *ctx));
extern void	blake2b512_final	__PR((UInt8_t[BLAKE2B_512_DIGEST_LENGTH], blake2b_ctx *ctx));

#ifdef	__cplusplus
}
#endif

#endif	/* _SCHILY_BLAKE2B_H */
